<?php

declare(strict_types=1);

namespace Drupal\datatables_server_side\Plugin\Block;

use Drupal\Component\Utility\Html;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\views\Entity\View;
use Drupal\views\Views;

/**
 * Provides a DataTables server side block.
 *
 * @Block(
 *   id = "datatables_server_side_block",
 *   admin_label = @Translation("DataTables server side"),
 *   category = @Translation("Custom")
 * )
 *
 * @noinspection PhpUnused
 */
class DataTablesServerSide extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'view' => NULL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $viewList = $displayList = [];

    $views = Views::getAllViews();
    foreach ($views as $view) {
      $viewList[$view->id()] = $view->label();
    }

    $form['view'] = [
      '#type' => 'select',
      '#title' => $this->t('Select the view for this DataTable.'),
      '#options' => $viewList,
      '#default_value' => $this->configuration['view'] ?? NULL,
    ];

    // @todo Make this work dynamically.
    if ($this->configuration['view']) {
      $view = View::load($this->configuration['view']);
      $displaysList = $view->get('display');
      foreach ($displaysList as $display) {
        $displayList[$display['id']] = $display['id'];
      }

      $form['display'] = [
        '#type' => 'select',
        '#title' => $this->t('Select the display for the DataTable.'),
        '#options' => $displayList,
        '#default_value' => $this->configuration['display'],
      ];
    }

    $form['searchReturn'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Require pressing &lt;enter&gt;/&lt;return&gt; to search?'),
      '#default_value' => $this->configuration['searchReturn'],
    ];

    $layoutOptions = [
      'info' => $this->t('Info'),
      'pageLength' => $this->t('Page length'),
      'paging' => $this->t('Paging'),
      'search' => $this->t('Search'),
    ];
    $form['layout'] = [
      '#type' => 'options',
      '#title' => $this->t('Layout options'),
    ];
    $form['layout']['topStart'] = [
      '#type' => 'select',
      '#options' => $layoutOptions,
      '#title' => $this->t('Top start'),
      '#default_value' => $this->configuration['layout']['topStart'],
    ];
    $form['layout']['topEnd'] = [
      '#type' => 'select',
      '#options' => $layoutOptions,
      '#title' => $this->t('Top end'),
      '#default_value' => $this->configuration['layout']['topEnd'],
    ];
    $form['layout']['bottomStart'] = [
      '#type' => 'select',
      '#options' => $layoutOptions,
      '#title' => $this->t('Bottom start'),
      '#default_value' => $this->configuration['layout']['bottomStart'],
    ];
    $form['layout']['bottomEnd'] = [
      '#type' => 'select',
      '#options' => $layoutOptions,
      '#title' => $this->t('Bottom end'),
      '#default_value' => $this->configuration['layout']['bottomEnd'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    // Need getCompleteFormState() to get the id from the parent form.
    $allValues = $form_state->getCompleteFormState()->getValues();
    $this->configuration['machine_name'] = $allValues['id'];
    $this->configuration['html_class'] = Html::cleanCssIdentifier($allValues['id']);

    // The rest we can do normally.
    $values = $form_state->getValues();
    $this->configuration['view'] = $values['view'];
    $this->configuration['display'] = $values['display'];
    $this->configuration['searchReturn'] = $values['searchReturn'];
    $this->configuration['layout'] = $values['layout'];
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    // @todo Proper permissions?
    $condition = TRUE;
    return AccessResult::allowedIf($condition);
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $header = $build = [];

    // Ensure that the block is configured enough.
    if (!isset($this->configuration['view'])
    || !isset($this->configuration['display'])) {
      return $build;
    }

    try {
      $view = Views::getView($this->configuration['view']);
      $view->setDisplay($this->configuration['display']);
      if ($view->display_handler->options['style']['type'] !== 'datatables_server_side_serializer') {
        return $build;
      }
      $url = $view->display_handler->getUrl()->toString();

      // Don't attach datatable library in the views edit screen.
      if (!isset($view->live_preview)) {
        $build['#attached']['library'][] = 'datatables_server_side/datatables_server_side';
      }
      $build['#attached']['drupalSettings']['datatablesServerSide'] = [
        'url' => $url,
        'id' => $this->configuration['html_class'],
        'searchReturn' => $this->configuration['searchReturn'],
        'layout' => $this->configuration['layout'],
      ];

      // Build the list of fields for the table header.
      $fields = $view->display_handler->getOption('fields');
      foreach ($fields as $field_name => $field_info) {
        $header[$field_name] = $field_info['label'] ?: $field_info['id'];
      }

      $build['table'] = [
        '#type' => 'table',
        '#header' => $header,
        '#attributes' => [
          'class' => ['hover'],
          'id' => [$this->configuration['html_class']],
        ],
        '#cache' => [
          'max-age' => 0,
        ],
      ];
    }
    catch (\Exception $e) {
    }

    return $build;
  }

}
