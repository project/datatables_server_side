<?php

declare(strict_types=1);

namespace Drupal\datatables_server_side\Plugin\views\style;

use Drupal\rest\Plugin\views\style\Serializer;

/**
 * Views style plugin for DataTables server side.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *    id = "datatables_server_side_serializer",
 *    title = @Translation("DataTables server side"),
 *    help = @Translation("Display rows in a datatables.net Table using server side processing.."),
 *    display_types = {"data"}
 *  )
 *
 * @noinspection PhpUnused
 */
class DataTablesServerSideSerializer extends Serializer {

  /**
   * {@inheritdoc}
   */
  protected $usesOptions = TRUE;

  /**
   * {@inheritdoc}
   */
  public function render() {
    $rows = [];
    $rows['draw'] = $this->view?->draw ?? 1;
    $rows['recordsTotal'] = $this->view->total_rows;
    $rows['recordsFiltered'] = $this->view->total_rows;

    foreach ($this->view->result as $row) {
      $rowFields = [];
      foreach ($this->view->rowPlugin->render($row) as $rowField) {
        $rowFields[] = $rowField;
      }
      $rows['data'][] = $rowFields;
    }

    if (isset($this->view->live_preview) && $this->view->live_preview) {
      $output = json_encode($rows, JSON_PRETTY_PRINT);
    }
    else {
      $output = $this->serializer->serialize($rows, 'json', ['views_style_plugin' => $this]);
    }

    return $output;
  }

  /**
   * Return only json for formats.
   *
   * @return string[]
   *   An array of format options. Both key and value are the same.
   */
  protected function getFormatOptions() {
    return [
      'json' => 'json',
    ];
  }

}
