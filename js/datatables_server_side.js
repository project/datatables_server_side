(function ($, Drupal, drupalSettings, once) {
  Drupal.behaviors.DataTablesBlock = {
    attach(context, settings) {
      const dtss = drupalSettings.datatablesServerSide;
      $(once(`${dtss.id}-once`, `#${dtss.id}`, context)).each(function () {
        $(this).DataTable({
          ajax: dtss.url,
          type: "POST",
          processing: true,
          serverSide: true,
          ordering: true,
          searching: true,
          search: {
            return: dtss.searchReturn,
          },
          layout: {
            topStart: dtss.layout.topStart,
            topEnd: dtss.layout.topEnd,
            bottomStart: dtss.layout.bottomStart,
            bottomEnd: dtss.layout.bottomEnd,
          },
        });
      });
    },
  };
})(jQuery, Drupal, drupalSettings, once);
