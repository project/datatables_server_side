
# Datatables server side

An implementation of DataTables server side - https://datatables.net/manual/server-side
processing using Drupal views as the backend for configuring the data source,
filters and sortable fields.

This module provides:
- A block the integrates the http://datatables.net plugin into a Drupal Block
- A Views style plugin that works with the block to provide service side
  processing for datatables.
- An implementation of hook_views_pre_view() that does the heavy lifting of
  converting the DataTables requests into views settings.

# Using this module is a two step process:
## 1. Create the view with the data.
1. Create a view as normal.
2. Add a new REST export display.
3. In the *Format* section, select *DataTables server side*.
4. In settings, ensure *json* is selected.
5. Set the pager to *full*, 10 items.

## 2. Place an instance of the block.
1. Visit The Block layout page - admin/structure/block.
2. Click *Place block* in the region you want the block to appear.
3. Click *Place block* for the DataTables server side block.
4. In the *Configure block* modal, select the view created earlier.
5. Once thats selected, the display selection drop down will appear.
6. Select the REST export using the DataTables server side style.
7. Its a standard block, so any other settings should work as normal.

## Multiple block instances.
- This should work fine as long as the machine name for each block is different.

## Enable searching.
1. On the view, add Filter criteria for each field that should be included when the search box is used.
2. Typically using *Contains* will get the best results.
3. Leave the value empty, it will be filled from the DataTable request.
4. Do not set the filter to *exposed*.
5. The text will automatically be searched for across all the fields with a filter.
6. **Only basic text filters are supported**

## Enable sorting.
1. On the view, add Sort criteria for each field that sorting should be enabled for.
2. The Block will detect that there are sort fields and enable if for each related column.

## Project links.
- For a full description of the module, visit the project page:
  <https://www.drupal.org/project/datatables_server_side>
- To submit bug reports and feature suggestions, or to track changes:
  <https://www.drupal.org/project/issues/search/datatables_server_side>
- For more information about the DataTables plugin, full documentation and
  examples are available at: <http://datatables.net>

## Troubleshooting
- If no total count, or paging not working, ensure the pager is set to full
  on the view, or the total records isn't counted.
- Check console log for any JavaScript errors.
- Check access log to ensure that submitted requests are coming through.
- Check Watchdog log for errors.

## Credits
This module is inspired by:
- The DataTables module - https://www.drupal.org/project/datatables
- The DataTables CDN module - https://www.drupal.org/project/datatables_cdn
- The WebWash Block Plugin episode - https://www.youtube.com/live/TonsrjX2WJE?si=_Y90XS7WKL7zsDZ3
