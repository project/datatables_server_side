<?php

declare(strict_types=1);

use Drupal\views\ViewExecutable;

/**
 * Implements hook_views_pre_view().
 *
 * @noinspection PhpUnused
 */
function datatables_server_side_views_pre_view(ViewExecutable $view) {
  // FIELDS: DataTables server side requires fields.
  $hasFields = array_key_exists('fields', $view->display_handler->options);
  if (!$hasFields) {
    // Some sort of warning here?
    return;
  }

  // We only handle our own display handler.
  if (isset($view->display_handler->options['style']['type'])
  && $view->display_handler->options['style']['type'] !== 'datatables_server_side_serializer') {
    return;
  }

  $request = \Drupal::request();
  $params = $request->query->all();

  // Set the redraw count.
  $draw = $params['draw'] ?? 0;
  $view->draw = (int) $draw;

  // PAGING: Get the start and length to do paging.
  $start = $params['start'] ?? 0;

  // Can set page length with just this.
  if (isset($params['length'])) {
    $length = $params['length'] ?? 0;
    $view->getPager()->setItemsPerPage($length);
  }

  // Need both vars to convert from item to page.
  if (isset($length)) {
    $page = (int) ceil((int) $start / (int) $length);
    $view->getPager()->setCurrentPage($page);
  }

  // SORTING: Work out what column to sort on.
  if (isset($params['order'])) {
    $sort = $params['order'][0];
    $fullFields = $view->display_handler->getOption('fields');
    $fields = array_keys($fullFields);
    $colName = $fields[$sort['column']];

    $viewField = $fullFields[$colName];

    $sortHandlers = [
      $colName => [
        'order' => strtoupper($params['order'][0]['dir']),
        'id' => $viewField['id'],
        'table' => $viewField['table'],
        'field' => $viewField['field'],
        'relationship' => $viewField['relationship'],
      ],
    ];
    $view->display_handler->overrideOption('sorts', $sortHandlers);
  }

  // FILTERING: Work out filterable columns.
  if ($searchTerm = $params['search']['value'] ?? '') {
    $addedFilters = FALSE;
    $filterHandlers = $view->display_handler->getOption('filters');
    $filterGroups = $view->display_handler->getOption('filter_groups');
    $nextGroup = count($filterGroups['groups']) + 1;

    foreach ($view->display_handler->getOption('fields') as $field) {
      // Random string so we don't conflict with existing filters.
      $id = uniqid($field['id'] . '_', TRUE);
      $filterHandlers[$id] = [
        'id' => $id,
        'table' => $field['table'],
        'field' => $field['field'],
        'relationship' => $field['relationship'],
        'group_type' => $field['group_type'],
        'exposed' => FALSE,
        'operator' => 'contains',
        'value' => $searchTerm,
        'group' => $nextGroup,
        'entity_type' => $field['entity_type'],
        'entity_field' => $field['entity_field'],
        'plugin_id' => 'string',
      ];
      $addedFilters = TRUE;
    }

    // If we added filters, apply them.
    if ($addedFilters) {
      // Add the new 'or' type group to match on multiple fields.
      $filterGroups['groups'][$nextGroup] = 'OR';

      // Set the overrides.
      $view->display_handler->overrideOption('filter_groups', $filterGroups);
      $view->display_handler->overrideOption('filters', $filterHandlers);
    }
  }

}
